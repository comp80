<?php

define('COMP80', true);

require_once('comp80.php');
$Comp80 = new Comp80();

// Set timezone to East Coast
date_default_timezone_set('America/New_York');

if ($_GET['page'] == 'about')
	include('pages/about.php');
elseif ($_GET['page'] == 'resources')
	include('pages/resources.php');
elseif ($_GET['page'] == '')
	include('pages/home.php');