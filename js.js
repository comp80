function show_tweets () {
	$.get('pages/ajax_show_tweets.php', function(data) {
		$('#comp80-tweets').html(data);
	});
}

function hide_input_text (text) {
	var inputText = '';
	$('input[type=text]').focus(function() {
		if ($(this).val() == text) {
			inputText = $(this).val();
			$(this).val('');
		}
	}).blur(function() {
		if ($(this).val() == '')
			$(this).val(text);
	});
}