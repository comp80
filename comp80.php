<?php

if (!defined('COMP80')) { header('Location:/'); }

class Comp80 {
	
	// get_comp80_tweets() - gets tweets matching #comp80 hash tag from Twitter
	// 
	// @return String - json
	function get_comp80_tweets () {
		$ch = curl_init('http://search.twitter.com/search.json?q=%23comp80');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		$content = curl_exec($ch);
		$error   = curl_errno($ch);
		curl_close($ch);
		
		return ($error == 0) ? $content : false;
	}
	
	// get_tweets_array() - gets an array of tweets using get_comp80_tweets()
	//
	// @return Array - array of tweets
	function get_tweets_array () {
		return json_decode($this->get_comp80_tweets());
	}
	
	// show_tweets() - parse JSON object and return an tweets in HTML format
	function show_tweets () {
		$tweets = $this->get_tweets_array();
		$display = '';
		for ($index = 0; $index < sizeof($tweets->results); $index++) {
			$display .= '<div class="tweet">';
			
			// Display profile images
			$display .= '<img src="'.$tweets->results[$index]->profile_image_url.'" alt="Profile image" class="profile-img" />';
			
			// Display tweet
			$display .= '<p class="tweet">'.$tweets->results[$index]->text.'</p>';
			
			$display .= '<p class="tweet-info">';
			// Display creation time
			// 9:49 PM, March 9
			$display .= '<span class="date">'.date('g:i a F j, Y', strtotime($tweets->results[$index]->created_at)).'</span>';
			
			// Display usernames
			$display .= '<span class="from-user"> from <a href="http://twitter.com/'.$tweets->results[$index]->from_user.'">'.$tweets->results[$index]->from_user.'</a></span>';
			$display .= '</p>';
			$display .= '</div>';
		}
		
		return $display;
	}
	
}