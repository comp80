<?php include('pages/header.php'); ?>


<div id="about">
	<h2>About</h2>
	
	<p>This site displays the latest <a href="http://twitter.com">Twitter</a> tweets with the <a href="http://search.twitter.com/search?q=comp80">#comp80</a> hashtag.</p>
	<p>We also manage an IRC chatroom for discussion of anything relating to comp80.</p>
	
	<p><em>Comp80</em> is also open source, and is managed using <a href="http://www.git-scm.com/">git</a>. If you are interested in improving the site, feel free to fork the project and add to it.</p>
</div>


<?php include('pages/footer.php'); ?>