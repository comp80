<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Comp 80</title>

<link rel="stylesheet" media="screen" type="text/css" href="styles.css" />
<link rel="shortcut icon" href="images/favicon.gif" type="image/gif" />

<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="js.js"></script> -->
<!--
<script type="text/javascript">
// <![CDATA[
	$(document).ready(function() {
		//setInterval('show_tweets()', 1000);
		
		hide_input_text('Email');
		
		$('#irc > div').hide();
		$('#irc > h2').css('cursor', 'pointer').toggle(function() {
			$('#irc > div').slideDown();
		}, function() {
			$('#irc > div').slideUp();
		});
	});
// ]]>
</script>
-->

</head>
<body>

	<div id="wrap">
		
		<h1 id="header"><a href="/">COMP 80</a></h1>
		
		<div id="navigation">
			<ul>
				<li><a href="about">About</a></li>
				<li><a href="resources">Resources</a></li>
			</ul>
		</div>